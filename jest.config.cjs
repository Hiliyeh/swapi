module.exports = {
    setupFilesAfterEnv: ['./setupTests.ts'],
    testEnvironment: 'jest-environment-jsdom',
    transform: {
        '^.+\\.tsx?$': 'ts-jest',
        '^.+\\.css$': 'jest-css-modules-transform',
    },
};
