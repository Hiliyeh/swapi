import * as React from 'react';
import '@testing-library/jest-dom';
import { jest } from '@jest/globals';


window.React = React;
window.jest = jest;
