export interface FetchDataResult {
    data: Data | null;
    loading: boolean;
    error: Error | null;
    details: (url: string) => Promise<void>;
}
export interface Data {
    count: number;
    next: string | null;
    previous: string | null;
    results: Starship[];
}

export interface Starship {
    name: string;
    url: string;
    model?: string;
    manufacturer?: string;
    cost_in_credits?: string;
    length?: string;
    max_atmosphering_speed?: string;
    crew?: string;
    passengers?: string;
    cargo_capacity?: string;
    consumables?: string;
    hyperdrive_rating?: string;
    MGLT?: string;
    starship_class?: string;
}