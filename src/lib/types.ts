import {ChangeEvent} from "react";
import {Starship} from "./api";

export interface PaginationButtonProps {
    text: string;
    onClick: () => void;
    disabled: boolean;
    loading?: boolean;
}

export interface SearchInputProps {
    value: string;
    onChange: (event: ChangeEvent<HTMLInputElement>) => void;
    handleClearClick: () => void;
}
export interface InputChangeEvent extends React.ChangeEvent<HTMLInputElement> {}

export interface StarshipDetailsProps{
    activeStarship: Starship;
}

export interface StarshipListProps {
    starships: Starship[];
    activeStarship: Starship | null;
    onStarshipClick: (starship: Starship) => void;
}