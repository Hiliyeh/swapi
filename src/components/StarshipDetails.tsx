import {StarshipDetailsProps} from '../lib/types';

const StarshipDetails = ({ activeStarship }: StarshipDetailsProps) => {

    const starship = activeStarship ;

    return (
        <div>
            <h2>{starship.name}</h2>
            <p>Model: {starship.model}</p>
            <p>Manufacturer: {starship.manufacturer}</p>
            <p>Cost in Credits: {starship.cost_in_credits}</p>
            <p>Length: {starship.length}</p>
            <p>Max atmosphering speed: {starship.max_atmosphering_speed}</p>
            <p>Crew: {starship.crew}</p>
            <p>Passengers: {starship.passengers}</p>
            <p>Cargo Capacity: {starship.cargo_capacity}</p>
            <p>Consumables: {starship.consumables}</p>
            <p>Hyperdrive Rating: {starship.hyperdrive_rating}</p>
            <p>MGLT: {starship.MGLT}</p>
            <p>Starship Class: {starship.starship_class}</p>
        </div>
    );
};

export default StarshipDetails;
