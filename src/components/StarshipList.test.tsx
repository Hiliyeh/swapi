import { render, fireEvent, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import StarshipList from './StarshipList';

const mockStarshipList = require('../__mocks__/starshipListData.json');

describe('StarshipList', () => {
    const starships = mockStarshipList.results;

    test('displays all starships', () => {
        render(<StarshipList starships={starships} activeStarship={null} onStarshipClick={() => {}} />);

        expect(screen.getByText('CR90 corvette')).toBeInTheDocument();
        expect(screen.getByText('Star Destroyer')).toBeInTheDocument();
        expect(screen.getByText('Millennium Falcon')).toBeInTheDocument();
    });

    test('calls onStarshipClick when a starship is clicked', () => {
        const mockOnClick = jest.fn();
        render(<StarshipList starships={starships} activeStarship={null} onStarshipClick={mockOnClick} />);
        const starshipLink = screen.getByText('CR90 corvette');
        fireEvent.click(starshipLink);
        expect(mockOnClick).toHaveBeenCalledTimes(1);
        expect(mockOnClick).toHaveBeenCalledWith(starships[0]);
    });

    test('displays active starship', () => {
        const activeStarship = starships[1];
        render(<StarshipList starships={starships} activeStarship={activeStarship} onStarshipClick={() => {}} />);
        const activeLink = screen.getByText('Star Destroyer');
        fireEvent.click(activeLink);
        expect(activeLink).toHaveClass('active');
    });
});


