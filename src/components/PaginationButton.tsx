import {PaginationButtonProps} from '../lib/types';
const PaginationButton = ({ text, onClick, disabled, loading }: PaginationButtonProps) => {
    return (
        <button className="pagination-button" onClick={onClick} disabled={disabled || loading}>
            {loading ? 'Loading...' : text}
        </button>
    );
};

export default PaginationButton;
