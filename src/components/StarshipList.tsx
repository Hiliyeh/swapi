import {StarshipListProps} from '../lib/types';

import './StarshipList.css';

const StarshipList = ({ starships, activeStarship, onStarshipClick }: StarshipListProps) => {
    return (
        <ul>
            {starships?.map((starship) => (
                <li key={starship.name}>
                    <a
                        href={starship.url}
                        onClick={(e) => {
                            e.preventDefault();
                            onStarshipClick(starship);
                        }}
                        className={activeStarship === starship ? 'active' : ''}
                    >
                        {starship.name}
                    </a>
                </li>
            ))}
        </ul>
    );
};

export default StarshipList;