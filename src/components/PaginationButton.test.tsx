import {fireEvent, render} from '@testing-library/react';

import PaginationButton from './PaginationButton';
describe('PaginationButton', () => {

    test('renders button with text', () => {
        const { getByText } = render(
            <PaginationButton text="Le B" onClick={() => {}} disabled={false} />
        );
        const buttonElement = getByText('Le B');
        expect(buttonElement).toBeInTheDocument();

    });

    test('calls onClick when clicked', () => {
        const onClickMock = jest.fn();
        const { getByRole } = render(
            <PaginationButton text="Test Button" onClick={onClickMock} disabled={false} />
        );
        const buttonElement = getByRole('button');
        fireEvent.click(buttonElement);
        expect(onClickMock).toHaveBeenCalledTimes(1);
    });

    test('disables button when disabled prop is true', () => {
        const { getByRole } = render(
            <PaginationButton text="Test Button" onClick={() => {}} disabled={true} />
        );
        const buttonElement = getByRole('button');
        expect(buttonElement).toBeDisabled();
    });

    test('shows loading text when loading prop is true', () => {
        const { getByText } = render(
            <PaginationButton text="Test Button" onClick={() => {}} disabled={false} loading={true} />
        );
        const loadingTextElement = getByText("Loading...");
        expect(loadingTextElement).toBeInTheDocument();
    });
});