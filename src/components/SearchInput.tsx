import { SearchInputProps } from '../lib/types';

import './SearchInput.css';
const SearchInput = ({ value, onChange,handleClearClick }: SearchInputProps) => {

    return (
        <div>
            <input type="text" value={value} onChange={onChange} placeholder="Search" />
            {value && (
                <button onClick={handleClearClick}>
                    <i className="close-icon"></i>
                </button>
            )}
        </div>
    );
};

export default SearchInput;