import { render } from '@testing-library/react';
import { getByPlaceholderText } from '@testing-library/dom';
import userEvent from '@testing-library/user-event';

import SearchInput from './SearchInput';

describe('SearchInput', () => {

    test('renders input with initial value', () => {
        const mockHandleClearClick = jest.fn();
        const { getByDisplayValue } = render(<SearchInput value="initial value" onChange={() => {}} handleClearClick={mockHandleClearClick} />);
        const inputElement = getByDisplayValue('initial value');
        expect(inputElement).toBeInTheDocument();
    });

    test('calls onChange with input value when typed', async () => {
        const mockOnChange = jest.fn();
        const mockHandleClearClick = jest.fn();
        const { getByDisplayValue } = render(
            <SearchInput value="" onChange={mockOnChange} handleClearClick={mockHandleClearClick}/>
        );
        const inputElement = getByDisplayValue('');
        await userEvent.type(inputElement, 'test');
        expect(mockOnChange).toHaveBeenCalledTimes(4);

    });

    test('renders input with placeholder', () => {
        const mockHandleClearClick = jest.fn();
        const { getByPlaceholderText } = render(<SearchInput value="" onChange={() => {}} handleClearClick={mockHandleClearClick} />);
        const inputElement = getByPlaceholderText('Search');
        expect(inputElement).toBeInTheDocument();
    });
});
