import { render, screen } from '@testing-library/react';
import StarshipDetails from './StarshipDetails';

const mockStarship = require('../__mocks__/starshipDetailData.json');

describe('StarshipDetails', () => {

    test('renders the starship name', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipName = screen.getByText('TIE Advanced x1');
        expect(starshipName).toBeInTheDocument();
    });

    test('renders the starship model', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipModel = screen.getByText(/model: twin ion engine advanced x1/i);
        expect(starshipModel).toBeInTheDocument();
    });


    test('renders the starship manufacturer', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipManufacturer = screen.getByText(/manufacturer: sienar fleet systems/i)
        expect(starshipManufacturer).toBeInTheDocument();
    });

    test('renders the starship cost in credits', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipCost = screen.getByText(/cost in credits: unknown/i)
        expect(starshipCost).toBeInTheDocument();
    });

    test('renders the starship length', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipLength = screen.getByText(/length: 9\.2/i)
        expect(starshipLength).toBeInTheDocument();
    });

    test('renders the starship max atmosphering speed', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipSpeed = screen.getByText(/max atmosphering speed: 1200/i)
        expect(starshipSpeed).toBeInTheDocument();
    });

    test('renders the starship crew', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipCrew = screen.getByText(/crew: 1/i)
        expect(starshipCrew).toBeInTheDocument();
    });

    test('renders the starship passengers', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipPassengers = screen.getByText(/passengers: 0/i)
        expect(starshipPassengers).toBeInTheDocument();
    });

    test('renders the starship cargo capacity', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipCapacity = screen.getByText(/cargo capacity: 150/i)
        expect(starshipCapacity).toBeInTheDocument();
    });

    test('renders the starship consumables', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipConsumables = screen.getByText(/consumables: 5 days/i)
        expect(starshipConsumables).toBeInTheDocument();
    });
    test('renders the starship hyperdrive rating', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipHyperdrive = screen.getByText(/hyperdrive rating: 1\.0/i)
        expect(starshipHyperdrive).toBeInTheDocument();
    }
    );
    test('renders the starship MGLT', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipMGLT = screen.getByText(/MGLT: 105/i)
        expect(starshipMGLT).toBeInTheDocument();
    }
    );
    test('renders the starship starship class', () => {
        render(<StarshipDetails activeStarship={mockStarship}/>);
        const starshipClass = screen.getByText(/starship class: starfighter/i)
        expect(starshipClass).toBeInTheDocument();
    } );
});