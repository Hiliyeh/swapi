import React, { useCallback, useMemo, useState } from 'react';

import PaginationButton from './components/PaginationButton';
import StarshipList from './components/StarshipList';
import useFetchData from './hooks/useFetchData';
import StarshipDetails from './components/StarshipDetails';
import SearchInput from './components/SearchInput';

import {Starship} from './lib/api'
import {InputChangeEvent} from "./lib/types";
import { API_URL } from './utils/constants';

import './styles/App.css';

const App = () => {
    const [currentPage, setCurrentPage] = useState<number>(1);
    const [searchTerm, setSearchTerm] = useState<string>('');
    const [activeStarship, setActiveStarship] = useState<Starship | null>(null);

    const { data, loading, error, details } = useFetchData(API_URL, searchTerm, currentPage);

    const maxPageCount = useMemo(() => {
        return Math.ceil((data?.count ?? 0) / (data?.results?.length ?? 1));
    }, [data?.count]);

    const handleClearClick = useCallback(() => {
       setSearchTerm('');
        setCurrentPage(1);
        setActiveStarship(null);
    }, []);
    const handleNextPage = useCallback(() => {
        setCurrentPage(currentPage + 1);
        setActiveStarship(null);
    }, [currentPage]);

    const handlePrevPage = useCallback(() => {
        setCurrentPage(currentPage - 1);
        setActiveStarship(null);
    }, [currentPage]);

    const handleSearch = useCallback((event: InputChangeEvent) => {
        setSearchTerm(event.target.value);
        setCurrentPage(1);
        setActiveStarship(null);
    }, []);

    const handleStarshipClick = useCallback(async (starship: Starship) => {
        setActiveStarship(starship);
        await details(starship.url);
    }, [details]);

    const starshipList = useMemo(() => {
        return data ? data?.results : [];
    }, [data]);

    return (
        <div className="App">
            {loading && <div>Loading...</div>}
            {error && <div>{error.message}</div>}
            {data && (
                <div className="container">
                    <div className="core">
                    <h1>Star Wars Starships</h1>
                       <SearchInput value={searchTerm} onChange={handleSearch} handleClearClick={handleClearClick} />
                    <StarshipList
                        starships={starshipList}
                        activeStarship={activeStarship}
                        onStarshipClick={handleStarshipClick}
                    />
                    <PaginationButton
                        text="Previous"
                        onClick={handlePrevPage}
                        disabled={currentPage === 1 || searchTerm.length > 0 }
                    />
                    &nbsp;
                    {currentPage}
                    &nbsp;
                    <PaginationButton
                        text="Next"
                        onClick={handleNextPage}
                        disabled={currentPage === maxPageCount || searchTerm.length > 0}
                    />
                    </div>
                    {activeStarship && (
                        <StarshipDetails activeStarship={activeStarship} />
                    )}
                </div>
            )}
        </div>
    );
};

export default App;
