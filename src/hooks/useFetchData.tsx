import { useEffect, useState, useCallback } from 'react';

import {Data, FetchDataResult, Starship} from '../lib/api';

const useFetchData = (url: string, search: string, currentPage: number): FetchDataResult => {
    const [data, setData] = useState<Data | null>(null);
    const [loading, setLoading] = useState<boolean>(true);
    const [error, setError] = useState<Error | null>(null);
    const [details, setDetails] = useState<Starship | null>(null);

    const fetchData = useCallback(async () => {
        try {
            const response = await fetch(`${url}?search=${search}&page=${currentPage}`);
            const jsonData = await response.json();
            setData(jsonData);
            setLoading(false);
            setError(null);
        } catch (error:any) {
            setError(error);
            setLoading(false);
        }
    }, [url, search, currentPage]);

    const fetchDetails = useCallback(async (url: string) => {
        try {
            const response = await fetch(url);
            const jsonData = await response.json();
            setDetails(jsonData);
        } catch (error:any) {
            setError(error);
        }
    }, []);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    return {
        data: data,
        loading: loading,
        error: error,
        details: fetchDetails,
    };
};

export default useFetchData;
