# Star Wars Starships

This application displays a list of starships from the Star Wars universe, allowing the user to search through them and view additional details on each individual ship. The app is built using React and utilizes the SWAPI (Star Wars API) to retrieve data on the various starships

## Installation

```
git clone https://github.com/Hiliyeh/swapi.git

cd swapi-react

npm install
```

## Usage

```
npm run dev 
```

## Features

The Star Wars Starships application provides the following features:

- View a list of starships with basic information (name, manufacturer, cost, length, etc.)
- Search for starships by name
- Paginate through the list of starships
- Click on a starship to view additional details (model, crew, passengers, etc.)